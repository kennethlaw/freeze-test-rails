# Puma can serve each request in a thread from an internal thread pool.
# The `threads` method setting takes two numbers: a minimum and maximum.
# Any libraries that use thread pools should be configured to match
# the maximum value specified for Puma. Default is set to 5 threads for minimum
# and maximum; this matches the default thread size of Active Record.
#
threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }
threads threads_count, threads_count

# Specifies the `port` that Puma will listen on to receive requests; default is 3000.
#
port        ENV.fetch("PORT") { 3000 }

# Specifies the `environment` that Puma will run in.
#
environment ENV.fetch("RAILS_ENV") { "development" }

# Specifies the `pidfile` that Puma will use.
pidfile ENV.fetch("PIDFILE") { "tmp/pids/server.pid" }

# Specifies the number of `workers` to boot in clustered mode.
# Workers are forked webserver processes. If using threads and workers together
# the concurrency of the application would be max `threads` * `workers`.
# Workers do not work on JRuby or Windows (both of which do not support
# processes).
#
if ENV.include?("WEB_CONCURRENCY")
  workers ENV.fetch("WEB_CONCURRENCY")
end

# Use the `preload_app!` method when specifying a `workers` number.
# This directive tells Puma to first boot the application and load code
# before forking the application. This takes advantage of Copy On Write
# process behavior so workers use less memory.
#
# preload_app!

# Allow puma to be restarted by `rails restart` command.
plugin :tmp_restart

# Hack to increase stats collection interval
Puma::Cluster.const_set('WORKER_CHECK_INTERVAL', 0.5)

require 'webrick'
require 'json'

stats_server = WEBrick::HTTPServer.new(
  Port: '9394',
  BindAddress: '0.0.0.0',
  Logger: WEBrick::Log.new("/dev/null"),
  AccessLog: WEBrick::Log.new("/dev/null")
)

stats_server.mount_proc '/' do |req, res|
  if req.path == '/metrics'
    begin
      stats = JSON.parse(Puma.stats)
      worker_status = stats['worker_status']
      max_threads = worker_status.collect { |x| x['last_status']['max_threads'] }.sum
      ready_threads = worker_status.collect { |x| x['last_status']['pool_capacity'] }.sum
      res.status = 200
      prefix = ENV['THREAD_STATS_EXPORTER_PREFIX'] || 'puma'
      res.body = [
        "#{prefix}_max_threads{host=#{Socket.gethostname.to_json}} #{max_threads}",
        "#{prefix}_ready_threads{host=#{Socket.gethostname.to_json}} #{ready_threads}"
      ].join("\n")
    rescue => e
      res.status = 503
      res.body = 'Metrics to export are unavailable at the moment.'
    end
  else
    res.status = 404
    res.body = 'Not found. The exporter server only listens on /metrics'
  end
end
Thread.new { stats_server.start }
