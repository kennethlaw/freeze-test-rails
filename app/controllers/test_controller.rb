class TestController < ApplicationController
  def index
    # simulate coding defect which freezes the app
    will_sleep = params[:sleep]
    sleep 60 if will_sleep

    render html: "#{will_sleep ? 'a sleepy' : 'an energetic'} hello"
  end

  def healthz
    render status: :ok
  end
end
